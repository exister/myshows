//
//  NSString+Hash.h
//  MyShows
//
//  Created by Mikhail Kuznetsov on 22.10.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>

@interface NSString (Hash)

+ (NSString *) Md5Hash:(NSString *) string;

@end
