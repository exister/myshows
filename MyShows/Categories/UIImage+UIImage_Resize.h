//
//  UIImage+UIImage_Resize.h
//  MyShows
//
//  Created by Mikhail Kuznetsov on 06.11.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (UIImage_Resize)

+ (UIImage *)resizeImage:(UIImage *)image toSize:(CGSize)toSize;

@end
