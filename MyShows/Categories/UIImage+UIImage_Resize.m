//
//  UIImage+UIImage_Resize.m
//  MyShows
//
//  Created by Mikhail Kuznetsov on 06.11.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "UIImage+UIImage_Resize.h"

@implementation UIImage (UIImage_Resize)

+ (UIImage *)resizeImage:(UIImage *)image toSize:(CGSize)toSize {
    if (image.size.width != toSize.width && image.size.height != toSize.height) {
        UIGraphicsBeginImageContext(toSize);
        CGRect imageRect = CGRectMake(0.0, 0.0, toSize.width, toSize.height);
        [image drawInRect:imageRect];
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    return image;
}

@end
