//
//  ImageManager.m
//  IOSBoilerplate
//
//  Copyright (c) 2011 Alberto Gimeno Brieba
//  
//  Permission is hereby granted, free of charge, to any person
//  obtaining a copy of this software and associated documentation
//  files (the "Software"), to deal in the Software without
//  restriction, including without limitation the rights to use,
//  copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following
//  conditions:
//  
//  The above copyright notice and this permission notice shall be
//  included in all copies or substantial portions of the Software.
//  
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//  OTHER DEALINGS IN THE SOFTWARE.
//  

#import "ImageManager.h"
#import "ASIHTTPRequest.h"
#import "AppDelegate.h"

@implementation ImageManager

@synthesize cache = _cache;

- (id)init
{
    self = [super init];
    if (self) {
        pendingImages = [[NSMutableArray alloc] initWithCapacity:10];
        loadedImages = [[NSMutableDictionary alloc] initWithCapacity:50];
        downloadQueue = [[NSOperationQueue alloc] init];
        [downloadQueue setMaxConcurrentOperationCount:3];
    }
    
    return self;
}

static ImageManager *sharedSingleton;

+ (void)initialize {
    static BOOL initialized = NO;
    if(!initialized) {
        initialized = YES;
        sharedSingleton = [[ImageManager alloc] init];
    }
}

- (NSString*) cacheDirectory {
	return [NSHomeDirectory() stringByAppendingString:@"/Library/Caches/images/"];
}

+ (UIImage*)loadImage:(NSURL *)url userInfo:(NSDictionary *) userInfo {
    return [sharedSingleton loadImage:url userInfo:userInfo];
}

- (UIImage*)loadImage:(NSURL *)url userInfo:(NSDictionary *) userInfo {
	// NSLog(@"url = %@", url);
	UIImage* img = [loadedImages objectForKey:url];
    if (img) {
        return img;
    }
    
    if ([pendingImages containsObject:url]) {
        // already being downloaded
        return nil;
    }
    
    [pendingImages addObject:url];
    
    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];

    if (!self.cache) {
        self.cache = [[ASIDownloadCache alloc] init];
        [self.cache setStoragePath:[self cacheDirectory]];
    }
    [request setDownloadCache:self.cache];
    [request setCachePolicy:ASIOnlyLoadIfNotCachedCachePolicy];
    [request setCacheStoragePolicy:ASICachePermanentlyCacheStoragePolicy];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(imageDone:)];
    [request setDidFailSelector:@selector(imageWentWrong:)];
    [request setUserInfo:userInfo];
    [downloadQueue addOperation:request];
    return nil;
}

- (void)imageDone:(ASIHTTPRequest*)request {
	UIImage* image = [UIImage imageWithData:[request responseData]];
	if (!image) {
		return;
	}
	
	// NSLog(@"image done. %@ cached = %d", request.originalURL, [request didUseCachedResponse]);
	
	[pendingImages removeObject:request.originalURL];
	[loadedImages setObject:image forKey:request.originalURL];
    
    if ([[request userInfo] objectForKey:@"delegate"]) {
        id delegate = [[request userInfo] objectForKey:@"delegate"];
        
        SEL selector = @selector(imageLoaded:);
        if ([delegate respondsToSelector:selector]) {
            [delegate performSelector:selector withObject:image withObject:[request userInfo]];
        }
    }
}

- (void)imageWentWrong:(ASIHTTPRequest*)request {
	NSLog(@"image went wrong %@", [[request error] localizedDescription]);
	[pendingImages removeObject:request.originalURL]; // TODO should not try to load the image again for a while
}

+ (void) clearMemoryCache {
    [sharedSingleton clearMemoryCache];
}

- (void) clearMemoryCache {
	[loadedImages removeAllObjects];
	[pendingImages removeAllObjects];
}

+ (void) clearCache {
    [sharedSingleton clearCache];
}

- (void) clearCache {
    NSFileManager* fs = [NSFileManager defaultManager];
	// BOOL b = 
	[fs removeItemAtPath:[self cacheDirectory] error:NULL];
    
}

@end
