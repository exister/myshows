//
//  MyShowsApi.h
//  MyShows
//
//  Created by Mikhail Kuznetsov on 16.10.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequestDelegate.h"

@class SBJsonParser;

@protocol ApiDelegate <NSObject>
- (void) didRequestSucceed:(NSDictionary *)data;
- (void) didRequestFailed:(NSDictionary *)data;
@end


@interface MyShowsApi : NSObject <ASIHTTPRequestDelegate> {
    NSString *baseUrl;
    NSDictionary *urlsMap;
    NSString *responseString;
    
    SBJsonParser *parser;
}

@property(retain, nonatomic) id responder;

- (void)initUrlsMap;
- (NSString *)dictToQueryString:(NSDictionary *) params;
- (NSString *)getUrlForResource:(NSString *)resource;
- (void)sendRequest:(NSString *)resource queryParams:(NSDictionary *)queryParams;
- (void)sendRequest:(NSURL *)url resource:(NSString *)resource queryParams:(NSDictionary *)queryParams;
- (void)sendRequest:(NSString *)resource path:(NSString *)path queryParams:(NSDictionary *)queryParams;

@end
