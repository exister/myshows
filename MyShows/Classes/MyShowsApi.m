//
//  MyShowsApi.m
//  MyShows
//
//  Created by Mikhail Kuznetsov on 16.10.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "MyShowsApi.h"
#import "ASIHTTPRequest.h"
#import "SBJson.h"

@implementation MyShowsApi

@synthesize responder;

- (id)init {
    self = [super init];
    if (self) {
        baseUrl = @"http://api.myshows.ru/";
        [self initUrlsMap];
    }
    return self;
}

- (void) initUrlsMap {
    urlsMap = [[NSDictionary alloc] initWithObjectsAndKeys:
               @"profile/login", @"login",
               @"profile/shows/", @"shows",
               @"shows/%@", @"showDetails",
               nil];
}

- (NSString *) getUrlForResource:(NSString *)resource {
    if ([urlsMap objectForKey:resource]) {
        return (NSString *)[urlsMap objectForKey:resource];
    }
    return nil;
}

- (NSString *) dictToQueryString:(NSDictionary *) params 
{
    NSString *result = @"";
    
    if (params == nil){
        return result;
    }
    
    id key;
    NSEnumerator *enumerator = [params keyEnumerator];
    while (key = [enumerator nextObject]) {
        result = [result stringByAppendingFormat:@"%@=%@&", key, [params objectForKey:key]];
    }
    result = [result substringToIndex:[result length] - 1];
    return [result stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

- (void)sendRequest:(NSURL *)url resource:(NSString *)resource queryParams:(NSDictionary *)queryParams {
    NSLog(@"Requesting url is %@", url);
    SEL requestSelector = NSSelectorFromString([[NSString alloc] initWithFormat:@"request%@Finished:", [resource capitalizedString]]);
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    
    [request setDelegate:self];
    //NSLog(@"Checking for custom finish selector %@", requestSelector);
    if ([self respondsToSelector:requestSelector]) {
        NSLog(@"Setting custom finish selector");
        [request setDidFinishSelector:requestSelector];
    }
    [request startSynchronous];
}

- (void)sendRequest:(NSString *)resource queryParams:(NSDictionary *)queryParams {
    NSLog(@"Sending %@ request with params %@", resource, queryParams);
        
    NSURL *url = [NSURL URLWithString:[[NSString alloc] initWithFormat:@"%@%@?%@",
            baseUrl,
            [urlsMap objectForKey:resource], 
            [self dictToQueryString:queryParams]
        ]
    ];
    
    [self sendRequest:url resource:resource queryParams:queryParams];
}

- (void)sendRequest:(NSString *)resource path:(NSString *)path queryParams:(NSDictionary *)queryParams {
    NSLog(@"Sending %@ request with params %@", resource, queryParams);
    
    NSURL *url = [NSURL URLWithString:[[NSString alloc] initWithFormat:@"%@%@?%@",
                                       baseUrl,
                                       path, 
                                       [self dictToQueryString:queryParams]
                                       ]
                  ];
    
    [self sendRequest:url resource:resource queryParams:queryParams];
}

- (void) requestFinished:(ASIHTTPRequest *)request {
    NSLog(@"Request finished with %d status code", [request responseStatusCode]);
    
    /*NSHTTPCookie *cookie;
    NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [cookieJar cookies]) {
        NSLog(@"%@", cookie);
    }*/
    
    if ([request responseStatusCode] == 200) {
        NSLog(@"Storing response...");
        //NSLog(@"%@", [request responseString]);
        responseString = [request responseString]; 
    }
    else {
        NSLog(@"Request failed...");
        [[request delegate] requestFailed:request];
    }
}

- (void) requestFailed:(ASIHTTPRequest *)request {
    NSLog(@"Request failed with %d status code", [request responseStatusCode]);
    NSDictionary *data = [[NSDictionary alloc] initWithObjectsAndKeys:
                          [[NSNumber alloc] initWithInt:[request responseStatusCode]], @"status",
                          nil];
    [[self responder] didRequestFailed:data];
}

- (void) requestLoginFinished:(ASIHTTPRequest *)request {
    [self requestFinished:request];
    
    if ([request responseStatusCode] == 200 ||
        [request responseStatusCode] == 403 ||
        [request responseStatusCode] == 404) 
    {
        NSDictionary *data = [[NSDictionary alloc] initWithObjectsAndKeys:
                              [[NSNumber alloc] initWithInt:[request responseStatusCode]], @"status",
                              nil];
        NSLog(@"Checking for selector");    
        if ([[self responder] respondsToSelector:@selector(didRequestSucceed:)]) {
            NSLog(@"Responding to selector didRequestSucceed");
            [[self responder] didRequestSucceed:data];
        }
    }
}

- (void) requestShowsFinished:(ASIHTTPRequest *)request {
    [self requestFinished:request];
    
    if ([request responseStatusCode] == 200 ||
        [request responseStatusCode] == 410) 
    {
        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                              [[NSNumber alloc] initWithInt:[request responseStatusCode]], @"status",
                              nil];
        
        if ([request responseStatusCode] == 200) {
            parser = [[SBJsonParser alloc] init];
            id parsed = [parser objectWithString:responseString];
            if (parsed) {
                //NSLog(@"Shows loaded %@", parsed);
                NSLog(@"Shows loaded");
                [data setObject:[parsed copy] forKey:@"data"];
            }
            else {
                NSLog(@"Can't parse data %@", parser.error);
            }
        }
        
        NSLog(@"Checking for selector");    
        if ([[self responder] respondsToSelector:@selector(didRequestSucceed:)]) {
            NSLog(@"Responding to selector didRequestSucceed");
            [[self responder] didRequestSucceed:data];
        }
    }
}

- (void) requestShowdetailsFinished:(ASIHTTPRequest *)request {
    [self requestFinished:request];
    
    if ([request responseStatusCode] == 200) 
    {
        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                     [[NSNumber alloc] initWithInt:[request responseStatusCode]], @"status",
                                     nil];
        
        parser = [[SBJsonParser alloc] init];
        id parsed = [parser objectWithString:responseString];
        if (parsed) {
            //NSLog(@"Shows loaded %@", parsed);
            NSLog(@"Shows loaded");
            [data setObject:[parsed copy] forKey:@"data"];
        }
        else {
            NSLog(@"Can't parse data %@", parser.error);
        }

        NSLog(@"Checking for selector");    
        if ([[self responder] respondsToSelector:@selector(didRequestSucceed:)]) {
            NSLog(@"Responding to selector didRequestSucceed");
            [[self responder] didRequestSucceed:data];
        }
    }
}
@end
