//
//  TvShowCell.h
//  MyShows
//
//  Created by Mikhail Kuznetsov on 30.10.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TvShowCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *enTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *ruTitleLabel;
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) IBOutlet UILabel *watchedAllLabel;
@property (nonatomic, strong) IBOutlet UILabel *watchStatusLabel;
@property (nonatomic, strong) IBOutlet UILabel *showStatusLabel;
@property (nonatomic, strong) IBOutlet UIImageView *ratingImageView;
@end
