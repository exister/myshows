//
//  TvShowCell.m
//  MyShows
//
//  Created by Mikhail Kuznetsov on 30.10.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "TvShowCell.h"

@implementation TvShowCell

@synthesize enTitleLabel, ruTitleLabel, watchedAllLabel, watchStatusLabel, showStatusLabel, imageView, ratingImageView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
