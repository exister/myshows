//
//  ViewController.h
//  MyShows
//
//  Created by Mikhail Kuznetsov on 16.10.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyShowsApi.h"

@interface LoginViewController : UIViewController <ApiDelegate> {
    MyShowsApi *api;
}
@property (retain, nonatomic) IBOutlet UITextField *login;
@property (retain, nonatomic) IBOutlet UITextField *password;

- (IBAction)signInButtonPressed:(id)sender;
- (IBAction)signUpButtonPressed:(id)sender;

- (IBAction)closeKeyBoard:(id)sender;
@end
