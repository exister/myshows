//
//  ViewController.m
//  MyShows
//
//  Created by Mikhail Kuznetsov on 16.10.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "LoginViewController.h"
#import "NSString+Hash.h"
#import "SVProgressHUD.h"


@implementation LoginViewController
@synthesize login, password;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    api = [[MyShowsApi alloc] init];
    [api setResponder:self];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - ApiDelegate

- (void) didRequestSucceed:(NSDictionary *) data {
    NSLog(@"Returned to view after successful request with data %@", data);
    int status = [(NSNumber *)[data objectForKey:@"status"] intValue];
    
    if (status == 200 ) {
        [SVProgressHUD dismiss];
        [self performSegueWithIdentifier:@"login_to_main_controller" sender:nil];
    }
    else {
        [SVProgressHUD dismissWithError:@"Unknown error"];
    }
}

- (void) didRequestFailed:(NSDictionary *) data {
    int status = [(NSNumber *)[data objectForKey:@"status"] intValue];
    if (status == 404) {
        [SVProgressHUD dismissWithError:@"User not found"];
    }
    else if (status == 403) {
        [SVProgressHUD dismissWithError:@"Wrong credentials"];
    }
    else {
        [SVProgressHUD dismissWithError:@"Unknown error"];
    }
}

#pragma mark - Actions
- (IBAction)signInButtonPressed:(id)sender {
    NSLog(@"Signing in...");
    
    if ([login.text length] != 0 && [password.text length] != 0){
        NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:
                                login.text, @"login",
                                [NSString Md5Hash:password.text], @"password", 
                                nil];
        
        [SVProgressHUD showInView:self.view];
        [api sendRequest:@"login" queryParams:params];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Please, enter your username and password"
                              message:nil
                              delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)signUpButtonPressed:(id)sender {
    NSLog(@"Signing up...");
}

- (IBAction)closeKeyBoard:(id)sender {
    [login resignFirstResponder];
    [password resignFirstResponder];
}


@end
