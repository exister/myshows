//
//  ShowDetailViewController.h
//  MyShows
//
//  Created by Mikhail Kuznetsov on 06.11.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyShowsApi.h"
#import "ImageManager.h"

@class TvShow;

@interface ShowDetailViewController : UIViewController <ApiDelegate, AsyncImageDelegate> {
    MyShowsApi *api;
}

@property (nonatomic, retain) TvShow *show;

@property (retain, nonatomic) IBOutlet UILabel *ruTitle;
@property (retain, nonatomic) IBOutlet UILabel *showStatus;
@property (retain, nonatomic) IBOutlet UILabel *country;
@property (retain, nonatomic) IBOutlet UILabel *year;
@property (retain, nonatomic) IBOutlet UILabel *watching;
@property (retain, nonatomic) IBOutlet UILabel *rating;
@property (retain, nonatomic) IBOutlet UILabel *runtime;
@property (retain, nonatomic) IBOutlet UIImageView *imageView;

@end
