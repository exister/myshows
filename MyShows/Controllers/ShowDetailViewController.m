//
//  ShowDetailViewController.m
//  MyShows
//
//  Created by Mikhail Kuznetsov on 06.11.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "ShowDetailViewController.h"
#import "SVProgressHUD.h"
#import "TvShow.h"
#import "UIImage+UIImage_Resize.h"
#import "ImageManager.h"

@implementation ShowDetailViewController

@synthesize show, ruTitle, showStatus, country, year, watching, rating, runtime, imageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    api = [[MyShowsApi alloc] init];
    [api setResponder:self];
    
    [self.navigationItem setTitle:show.enTitle];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [SVProgressHUD showInView:self.view];

    [api sendRequest:@"showDetails" 
        path:[[NSString alloc] initWithFormat:[api getUrlForResource:@"showDetails"],
                                          [show.showId stringValue]
                                          ] 
         queryParams:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - ApiDelegate

- (void)didRequestSucceed:(NSDictionary *)data {
    NSLog(@"Returned to view after successful request");
    int status = [(NSNumber *)[data objectForKey:@"status"] intValue];
    
    if (status == 200 ) {
        [SVProgressHUD dismiss];
        
        NSDictionary *loaded_show = [data objectForKey:@"data"];
                
        if ([loaded_show isKindOfClass:[NSDictionary class]]){
            //[show updateWithDictionary:loaded_show];
            ruTitle.text = show.ruTitle;
            showStatus.text = show.showStatus;
            country.text = show.country;
            year.text = [show.year stringValue];
            watching.text = [show.watching stringValue];
            runtime.text = [show.runtime stringValue];
            
            UIImage *image;
            image = [ImageManager 
                     loadImage:[[NSURL alloc] initWithString:show.imageUrl] 
                     userInfo:[[NSDictionary alloc] initWithObjectsAndKeys:
                               self, @"delegate",
                               nil
                               ]
                     ];
            if (image) {
                image = [UIImage resizeImage:image toSize:CGSizeMake(139.0, 139.0)];
                show.image = image;
                imageView.image = image;
            }
            else {
                imageView.image = [UIImage imageNamed:@"tv_show_placeholder2.png"];
            }
        }        
    }
    else {
        if (status == 401) {
            [SVProgressHUD dismissWithError:@"Log in, please"];
        }
        else {
            [SVProgressHUD dismiss];
        }
    }
}

- (void)didRequestFailed:(NSDictionary *)data {
    
}

#pragma mark - AsyncImageDelegate

- (void) imageLoaded:(UIImage *)image userInfo:(NSDictionary *)userInfo {
    image = [UIImage resizeImage:image toSize:CGSizeMake(139.0, 139.0)];
    [show setImage:image];
    imageView.image = [show image];
}

@end
