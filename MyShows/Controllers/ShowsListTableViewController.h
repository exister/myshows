//
//  ShowsListTableViewController.h
//  MyShows
//
//  Created by Mikhail Kuznetsov on 22.10.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyShowsApi.h"
#import "ImageManager.h"

@class TvShow;

@interface ShowsListTableViewController : UITableViewController <ApiDelegate, AsyncImageDelegate> {
    MyShowsApi *api;
    NSMutableArray *shows;
    
    IBOutlet UITableView *showsTable;
}

@property (nonatomic, retain)NSIndexPath *detailPath;

- (UIImage *)loadImage:(TvShow *)currentShow indexPath:(NSIndexPath *) indexPath;
- (UIImage *)resizeImage:(UIImage *)image;
- (void)loadImagesForOnscreenRows;
- (UIImage *)imageForRating:(NSNumber *)rating;
@end
