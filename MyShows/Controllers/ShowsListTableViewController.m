//
//  ShowsListTableViewController.m
//  MyShows
//
//  Created by Mikhail Kuznetsov on 22.10.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "ShowsListTableViewController.h"
#import "SVProgressHUD.h"
#import "TvShow.h"
#import "TvShowCell.h"
#import "ShowDetailViewController.h"
#import "UIImage+UIImage_Resize.h"


@implementation ShowsListTableViewController

@synthesize detailPath;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    api = [[MyShowsApi alloc] init];
    [api setResponder:self];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [SVProgressHUD showInView:self.view];
    [api sendRequest:@"shows" queryParams:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - ApiDelegate

- (void)didRequestSucceed:(NSDictionary *)data {
    NSLog(@"Returned to view after successful request");
    int status = [(NSNumber *)[data objectForKey:@"status"] intValue];
    
    if (status == 200 ) {
        [SVProgressHUD dismiss];
        
        id key;
        NSDictionary *loaded_shows = [[data objectForKey:@"data"] copy];
        NSEnumerator *enumerator = [loaded_shows keyEnumerator];
        
        shows = [[NSMutableArray alloc] init];
        
        while (key = [enumerator nextObject]) {            
            if ([[loaded_shows objectForKey:key] isKindOfClass:[NSDictionary class]]){
                TvShow *tvShow = [[TvShow alloc] initWithDictionary:[loaded_shows objectForKey:key]];
                [shows addObject:tvShow];
            }
        }
        NSLog(@"Data stored in array %d, %@", [shows count], shows);
        
        [showsTable reloadData];
    }
    else {
        if (status == 401) {
            [SVProgressHUD dismissWithError:@"Log in, please"];
        }
        else {
            [SVProgressHUD dismiss];
        }
    }
}

- (void)didRequestFailed:(NSDictionary *)data {
    
}

#pragma mark - AsyncImageDelegate

- (void) imageLoaded:(UIImage *)image userInfo:(NSDictionary *)userInfo {
    if ([userInfo objectForKey:@"indexPath"]){
        image = [UIImage resizeImage:image toSize:CGSizeMake(61.0, 61.0)];
        [(TvShow *)[shows objectAtIndex:(NSUInteger)[[userInfo objectForKey:@"indexPath"] row]] setImage:image];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:(NSIndexPath *)[userInfo objectForKey:@"indexPath"]];
        cell.imageView.image = [(TvShow *)[shows objectAtIndex:(NSUInteger)[[userInfo objectForKey:@"indexPath"] row]] image];
    }
}

#pragma mark - Custom methods

- (void)loadImagesForOnscreenRows {
    if ([shows count] > 0) {
        for (NSIndexPath *indexPath in [self.tableView indexPathsForVisibleRows]) {
            TvShow *currentShow = [shows objectAtIndex:indexPath.row];
            if (!currentShow.image) {
                UIImage *image = [self loadImage:currentShow indexPath:indexPath];
                if (image) {
                    [self imageLoaded:image 
                             userInfo:[[NSDictionary alloc] initWithObjectsAndKeys:
                                                      indexPath, @"indexPath",
                                                      nil
                                       ]];
                }
            }
        }
    }
}

- (UIImage *)loadImage:(TvShow *)currentShow indexPath:(NSIndexPath *) indexPath {
    return [ImageManager 
     loadImage:[[NSURL alloc] initWithString:currentShow.imageUrl] 
     userInfo:[[NSDictionary alloc] initWithObjectsAndKeys:
               self, @"delegate",
               indexPath, @"indexPath",
               nil
               ]
     ];
}

- (UIImage *)imageForRating:(NSNumber *)rating
{
	switch (rating.intValue)
	{
		case 1: return [UIImage imageNamed:@"1starsmall.png"];
		case 2: return [UIImage imageNamed:@"2starsmall.png"];
		case 3: return [UIImage imageNamed:@"3starsmall.png"];
		case 4: return [UIImage imageNamed:@"4starsmall.png"];
		case 5: return [UIImage imageNamed:@"5starsmall.png"];
	}
	return nil;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSLog(@"Rows in section %d", [shows count]);
    return [shows count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TvShowCell";
    
    TvShowCell *cell = (TvShowCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...    
    TvShow *currentShow = [shows objectAtIndex:[indexPath row]];
         
    if (currentShow.ruTitle != (id)[NSNull null] && currentShow.ruTitle != nil && currentShow.ruTitle.length != 0) {
        cell.ruTitleLabel.text = currentShow.ruTitle;
    }
    else {
        cell.ruTitleLabel.text = @"";
    }
    
    cell.enTitleLabel.text = currentShow.enTitle;
    
    cell.watchedAllLabel.text = [NSString stringWithFormat:@"%d/%d", 
                                 currentShow.watchedEpisodes.intValue, 
                                 currentShow.totalEpisodes.intValue];
    cell.showStatusLabel.text = currentShow.showStatus;
    cell.watchStatusLabel.text = currentShow.watchStatus;
    if (currentShow.ratingValue > 0) {
        cell.ratingImageView.image = [self imageForRating:currentShow.ratingValue];
    }
    else {
        cell.ratingImageView.image = nil;
    }
    
    if (!currentShow.image) {
        UIImage *image;
        if (self.tableView.dragging == NO && self.tableView.decelerating == NO) {
            image = [self loadImage:currentShow indexPath:indexPath];
        }
        if (image) {
            image = [UIImage resizeImage:image toSize:CGSizeMake(61.0, 61.0)];
            currentShow.image = image;
            cell.imageView.image = image;
        }
        else {
            cell.imageView.image = [UIImage imageNamed:@"tv_show_placeholder2.png"];
        }
    }
    else {
        cell.imageView.image = currentShow.image;
    }
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0 || indexPath.row % 2 == 0) {
        cell.backgroundColor = [UIColor colorWithWhite:0.7 alpha:0.1];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        [self loadImagesForOnscreenRows];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self loadImagesForOnscreenRows];
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    self.detailPath = indexPath;
    [self performSegueWithIdentifier:@"show_detail" sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"show_detail"]) {
        ShowDetailViewController *controller = (ShowDetailViewController *)segue.destinationViewController;
        [controller setShow:(TvShow *)[shows objectAtIndex:[self.detailPath row]]];
    }
    else {
        [super prepareForSegue:segue sender:sender];
    }
}

@end
