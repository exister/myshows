//
//  TvShow.h
//  MyShows
//
//  Created by Mikhail Kuznetsov on 29.10.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TvShow : NSObject {
    //JSON fields
    NSNumber *_showId;
    NSString *_ruTitle;
    NSString *_enTitle;
    NSString *_imageUrl;
    NSString *_showStatus;
    NSString *_watchStatus;
    NSNumber *_watchedEpisodes;
    NSNumber *_totalEpisodes;
    NSNumber *_ratingValue;
    NSNumber *_year;
    NSString *_country;
    NSNumber *_kinopoiskId;
    NSNumber *_tvrageId;
    NSNumber *_imdbId;
    NSNumber *_watching;
    NSNumber *_runtime;
    
    //Object fields
    UIImage *_image;
    UIImage *_rating;
}

@property (nonatomic, retain) NSNumber *showId;
@property (nonatomic, retain) NSString *ruTitle;
@property (nonatomic, retain) NSString *enTitle;
@property (nonatomic, retain) NSString *imageUrl;
@property (nonatomic, retain) NSString *showStatus;
@property (nonatomic, retain) NSString *watchStatus;
@property (nonatomic, retain) NSNumber *watchedEpisodes;
@property (nonatomic, retain) NSNumber *totalEpisodes;
@property (nonatomic, retain) NSNumber *ratingValue;
@property (nonatomic, retain) NSNumber *year;
@property (nonatomic, retain) NSString *country;
@property (nonatomic, retain) NSNumber *kinopoiskId;
@property (nonatomic, retain) NSNumber *tvrageId;
@property (nonatomic, retain) NSNumber *imdbId;
@property (nonatomic, retain) NSNumber *watching;
@property (nonatomic, retain) NSNumber *runtime;

@property (nonatomic, retain) UIImage *rating;
@property (nonatomic, retain) UIImage *image;

- (id)initWithDictionary:(NSDictionary *)data;
- (NSDictionary *)fieldMap;

@end
