//
//  TvShow.m
//  MyShows
//
//  Created by Mikhail Kuznetsov on 29.10.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "TvShow.h"

@implementation TvShow

@synthesize showId = _showId;
@synthesize ruTitle = _ruTitle;
@synthesize enTitle = _enTitle;
@synthesize imageUrl = _imageUrl;
@synthesize showStatus = _showStatus;
@synthesize watchStatus = _watchStatus;
@synthesize totalEpisodes = _totalEpisodes;
@synthesize watchedEpisodes = _watchedEpisodes;
@synthesize ratingValue = _ratingValue;
@synthesize country = _country;
@synthesize year = _year;
@synthesize kinopoiskId = _kinopoiskId;
@synthesize tvrageId = _tvrageId;
@synthesize imdbId = _imdbId;
@synthesize watching = _watching;
@synthesize runtime = _runtime;
@synthesize rating = _rating;
@synthesize image = _image;

- (id)initWithDictionary:(NSDictionary *)data {
    self = [super init];
    if (self) {
        NSLog(@"%@", data);
        NSLog(@"%@", [[data objectForKey:@"rating"] class]);
        NSDictionary *fieldMap = [self fieldMap];
        NSEnumerator *enumerator = [fieldMap keyEnumerator];
        id key;
        while (key = [enumerator nextObject]) {
            if ([data objectForKey:key]) {
                SEL s = NSSelectorFromString([[NSString alloc] 
                    initWithFormat:@"set%@%@:", 
                        [[(NSString *)[fieldMap objectForKey:key] substringToIndex:1]uppercaseString],
                        [(NSString *)[fieldMap objectForKey:key] substringFromIndex:1]
                    ]);
                [self performSelector:s withObject:[data objectForKey:key]];
            }
        }
        NSLog(@"%@, %@", self.rating, self.totalEpisodes);
    }
    return self;
}

- (NSDictionary *)fieldMap {
    // object field - json field
    return [NSDictionary dictionaryWithObjectsAndKeys:
            @"showId", @"showId",
            @"ruTitle", @"ruTitle",
            @"enTitle", @"title",
            @"imageUrl", @"image",
            @"showStatus", @"showStatus",
            @"watchStatus", @"watchStatus",
            @"watchedEpisodes", @"watchedEpisodes",
            @"totalEpisodes", @"totalEpisodes",
            @"ratingValue", @"rating",
            nil];
}

@end
